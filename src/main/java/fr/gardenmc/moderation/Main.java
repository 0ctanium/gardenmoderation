package fr.gardenmc.moderation;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import fr.gardenmc.moderation.command.Moderation;
import fr.gardenmc.moderation.command.completion.ModerationCompletion;
import fr.gardenmc.moderation.config.MessagesConfig;
import fr.gardenmc.moderation.config.ModerationConfig;
import fr.gardenmc.moderation.listener.Listeners;
import fr.gardenmc.moderation.manager.PlayerManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.permission.Permission;

public class Main extends JavaPlugin {

    private static final Logger log = Logger.getLogger("Minecraft");
    private static Main INSTANCE = null;
    private static Permission perms = null;
	public static YamlConfiguration LANG;
	public static File LANG_FILE;
	public static File transactionsLoggerFile;
	
	@Override
	public void onEnable() {	
		INSTANCE = this;
		init();
        
        log.info("[§6Garden§f] §aEnabling Moderation plugin !");

        this.getCommand("moderation").setExecutor(new Moderation(this));
        
        this.getCommand("moderation").setTabCompleter(new ModerationCompletion());
        
        getServer().getPluginManager().registerEvents(new Listeners(), this);
        
		new BukkitRunnable(){
		    @Override
		    public void run(){
		    	PlayerManager.getPlayers().forEach((uuid, data) -> {
		    		Player player = Bukkit.getPlayer(uuid);
		    		
		    		if(player == null) return;

		    		if(data.getData().getBoolean("data.state")) {
		    			player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.translateAlternateColorCodes('&', MessagesConfig.MODERATION_ON_TITLE.toString())));
		    		}
		    	});
		    }
		}.runTaskTimer(this, 0L, 1L);
	}
	
	@Override
	public void onDisable() {
		log.info("[§6Garden§f] §cDisabling Moderation plugin.");
	    log.info(String.format("[%s] Disabled Version %s", getDescription().getName(), getDescription().getVersion()));
	}
    
    /* VAULT */    
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
    
    public static Permission getPermissions() {
        return perms;
    }
    
	public static Logger getConsole() {
		return log;
	}
	
	public void initData() {
        File dataFolder = new File(Main.getInstance().getDataFolder(), "data/");
        if(!dataFolder.exists())
        	dataFolder.mkdir();
	}
	
    /* MESSAGES */
	public void loadLang() {
	    File lang = new File(getDataFolder(), "messages.yml");
	    if (!lang.exists()) {
	        try {
	            getDataFolder().mkdir();
	            lang.createNewFile();
	            File defConfigStream = new File("plugins/Garden-Moderation", "messages.yml");
	            if (defConfigStream != null) {
	                YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
	                defConfig.save(lang);
	                MessagesConfig.setFile(defConfig);
	            }
	        } catch(IOException e) {
	            e.printStackTrace(); // So they notice
	            System.err.println("[Garden-Moderation] Couldn't create messages file.");
	            System.err.println("[Garden-Moderation] This is a fatal error. Now disabling");
	            this.setEnabled(false); // Without it loaded, we can't send them messages
	        }
	    }
	    YamlConfiguration conf = YamlConfiguration.loadConfiguration(lang);
	    for(MessagesConfig item:MessagesConfig.values()) {
	        if (conf.getString(item.getPath()) == null) {
	            conf.set(item.getPath(), item.getDefault());
	        }
	    }
	    MessagesConfig.setFile(conf);
	    Main.LANG = conf;
	    Main.LANG_FILE = lang;
	    try {
	        conf.save(lang);
	    } catch(IOException e) {
	    	System.err.println("Garden-Moderation: Failed to save messages.yml.");
	    	System.err.println("Garden-Moderation: Report this stack trace to <your name>.");
	        e.printStackTrace();
	    }
	}

	public YamlConfiguration getLang() {
	    return LANG;
	}
	
    public boolean init() {
		try {
			saveDefaultConfig();		
			reloadConfig();	
			loadLang();
			initData();
			setupPermissions();
			PlayerManager.load();
			new ModerationConfig(this);
			return true;
		} catch (Exception e) {
			System.out.println("Erreur lors du rechargement de la configuration : " + e);
			return false;
		}
    }
    
    public static Main getInstance() {
    	return INSTANCE;
    }
}
