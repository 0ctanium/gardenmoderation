package fr.gardenmc.moderation.config;

import java.util.Map;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;


public enum MessagesConfig {
	PREFIX("prefix", "&d[&bGarden&d]"),
	MODERATOR_MOD_OFF("modertator-mod-off", "%prefix%&d &bVous êtes sortis du mode modération."),
	MODERATOR_MOD_ON("modertator-mod-on", "%prefix%&d &bVous êtes entré en mode modération."),
	MODERATION_ON_TITLE("moderation-on-title", "&6Modération en cours"),
	CANNOT_COMMAND_PLAYMOD("cannot-execute-command", "&cVous ne pouvez pas executer cette commande car vous êtes en mode joueur. \n Faites &6/moderation on &cpour entrer en mode modération."),
	AUTOMATICLY_SWITCHED("automaticly-switched", "%prefix%&d &bVous êtes automatiquement entré en mode modération."),
	RELOAD_SUCCESS("reload-success", "%prefix%&d Configuration rechargé avec succàs."),
	RELOAD_FAILED("reload-failed", "%prefix%&c Erreur lors du rechargement de la configuration."),
	NO_PERMISSION("no-permission", "&cVous n'avez pas la permission de faire ceci."),
	OFFLINE_PLAYER("offline-player", "&cLe joueur n'est pas en ligne."),
	INVALID_ARGUMENT("invalid-argument", "&cVeuillez préciser un argument valide.");
  
	private String path;  
	private String def;
	private static YamlConfiguration LANG;
	  
    /**
    * Lang enum constructor.
    * @param path The string path.
    * @param start The default string.
    */
	MessagesConfig(String path, String start) {
        this.path = path;
        this.def = start;
    }
 
    /**
    * Set the {@code YamlConfiguration} to use.
    * @param config The config to set.
    */
    public static void setFile(YamlConfiguration config) {
    	MessagesConfig.LANG = config;
    }

	
	public String toString() {
		String r = LANG.getString(this.path, this.def);
		return ChatColor.translateAlternateColorCodes('&', r);
	}
	
	public String parse() {
		String r = LANG.getString(this.path, this.def);;
		r =r.replaceAll("%prefix%", PREFIX.toString());
		return ChatColor.translateAlternateColorCodes('&', r);
	}
	
	public String parse(Map<String, String> args) {
		String r = LANG.getString(this.path, this.def);;
		Set<String> argsType = args.keySet();
		for (int i = 0; i < argsType.size(); i++) {
			r = r.replaceAll("%" + argsType.toArray()[i] + "%", (String) args.get(argsType.toArray()[i]));
		} 
		r = r.replaceAll("%prefix%", PREFIX.toString());
		return ChatColor.translateAlternateColorCodes('&', r);
	}
    
    /**
    * Get the default value of the path.
    * @return The default value of the path.
    */
    public String getDefault() {
        return this.def;
    }
 
    /**
    * Get the path to the string.
    * @return The path to the string.
    */
    public String getPath() {
        return this.path;
    }
}