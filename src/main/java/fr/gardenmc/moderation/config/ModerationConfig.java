package fr.gardenmc.moderation.config;

import java.util.List;

import org.bukkit.GameMode;

import fr.gardenmc.moderation.Main;

public class ModerationConfig {

	private static Main main = null;
	
	public ModerationConfig(Main main) {
		ModerationConfig.main = main;
	}
	
	public static List<String> blacklistedCommands() {
		return main.getConfig().getStringList("blacklisted-commands");
	}
	
	public static boolean teleportToLocation() {
		return main.getConfig().getBoolean("teleport-to-location");
	}
	
	public static boolean forceCommandSwitch() {
		return main.getConfig().getBoolean("teleport-to-location");
	}
	
	public static int forceCommandSwitchDelay() {
		return main.getConfig().getInt("force-command-switch-delay");
	}
	
	public static GameMode defaultGamemode() {
		return GameMode.valueOf(main.getConfig().getString("default-gamemode"));
	}

}
