package fr.gardenmc.moderation.util.player;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.potion.PotionEffect;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

public class PotionUtil {

	public static String toBase64(Collection<PotionEffect> effects) {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

			dataOutput.writeInt(effects.size());
			
			for (PotionEffect effect : effects) {
				dataOutput.writeObject(effect);
			}
			dataOutput.close();
			return Base64Coder.encodeLines(outputStream.toByteArray());
		} catch (Exception e) {
			throw new IllegalStateException("Unable to save effect.", e);
		}
	}
	
	public static Collection<PotionEffect> fromBase64(String data) {
        Collection<PotionEffect> effects = new ArrayList<PotionEffect>();
        if (data == null || Base64Coder.decodeLines(data) == null) 
            return effects;

        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
        BukkitObjectInputStream dataInput = null;

        int size = 0;
        
        try {
            dataInput = new BukkitObjectInputStream(inputStream);
            size = dataInput.readInt();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        for (int i = 0; i < size; i++) {
            try {
            	effects.add((PotionEffect) dataInput.readObject());
            } catch (IOException | ClassNotFoundException e) {
                try { dataInput.close(); } catch (IOException e1) {}
                return null;
            }
        }

        try { dataInput.close(); } catch (IOException e1) {}

        return effects;
    }
	
	
}
