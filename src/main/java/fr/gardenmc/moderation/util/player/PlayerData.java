package fr.gardenmc.moderation.util.player;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import fr.gardenmc.moderation.Main;

public class PlayerData {

	private UUID uuid;
	private File folderLocation;
	
	private File playerFile;
	private FileConfiguration playerData;
	
	public PlayerData(Player player) {
		this.uuid = player.getUniqueId();		
		this.folderLocation = new File(Main.getInstance().getDataFolder(), "data/");
		
		findPlayerFile();
		findPlayerData();
	}
	
	public PlayerData(OfflinePlayer player) {
		this.uuid = player.getUniqueId();	
		this.folderLocation = new File(Main.getInstance().getDataFolder(), "data/");
		
		findPlayerFile();
		findPlayerData();
	}
	
	public PlayerData(UUID uuid) {
		this.uuid = uuid;	
		this.folderLocation = new File(Main.getInstance().getDataFolder(), "data/");
		
		findPlayerFile();
		findPlayerData();
	}
		
	private boolean findPlayerFile() {	
		this.playerFile = new File(folderLocation, uuid + ".yml");
		
		if(this.playerFile == null) return false;
		return true;
	}
	
	private boolean findPlayerData() {
		this.playerData = YamlConfiguration.loadConfiguration(this.playerFile);
		
		if(this.playerData == null)
			return false;
		
		return true;
	}
	
	public File getFile() {
		return this.playerFile;
	}
	
	public FileConfiguration getData() {
		return this.playerData;
	}
	
	public void saveData() {
		Main.getInstance().getServer().getScheduler().runTaskAsynchronously(Main.getInstance(), new Runnable() {
			@Override
			public void run() {
				try {
					playerData.save(playerFile);
				} catch (IOException e) {
					e.printStackTrace();					
				}
			}
		});
	}
	
	public void saveDataSync() {
		try {
			playerData.save(playerFile);
		} catch (IOException e) {
			e.printStackTrace();					
		}
	}
	
}
