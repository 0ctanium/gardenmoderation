package fr.gardenmc.moderation.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.gardenmc.moderation.Main;
import fr.gardenmc.moderation.command.moderation.Toggle;
import fr.gardenmc.moderation.config.MessagesConfig;

public class Moderation implements CommandExecutor {

	private Main main = null;
	
	public Moderation(Main main) {
		this.main = main;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(args.length > 0) {
				switch (args[0]) {
				case "reload":
					if(sender.hasPermission("moderation.reload")) {
						if(main.init()) {
							sender.sendMessage(MessagesConfig.RELOAD_SUCCESS.parse());
						} else {
							sender.sendMessage(MessagesConfig.RELOAD_FAILED.parse());
						}
					} else {
						sender.sendMessage(MessagesConfig.NO_PERMISSION.parse());
					}
					break;
					
				case "toggle":
					return new Toggle(player).toggle();
					
				case "on":
					return new Toggle(player).on();
					
				case "off":
					return new Toggle(player).off();
					
				default:
					player.sendMessage(MessagesConfig.INVALID_ARGUMENT.parse());
					break;
				}
				return true;
			}
			return true;
		}
		return false;
	}
}
