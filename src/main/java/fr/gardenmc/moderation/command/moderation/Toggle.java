package fr.gardenmc.moderation.command.moderation;

import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import fr.gardenmc.moderation.config.MessagesConfig;
import fr.gardenmc.moderation.manager.PlayerManager;
import fr.gardenmc.moderation.util.player.PlayerData;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Toggle {
	private Player player = null;
	
	public Toggle(Player player) {
		this.player = player;

	}

	
	public Toggle(OfflinePlayer player) {		
		this.player = player.getPlayer();
	}
	
	public boolean toggle() {
		if(player != null) {
			if(player.hasPermission("moderation.toggle")) {
				PlayerData playerData = PlayerManager.getPlayerData(this.player.getUniqueId(), false);
				if(playerData == null) return false;
				FileConfiguration data = playerData.getData();
				boolean state = data == null ? false : data.getBoolean("data.state");
				
				if(!state) {
					player.sendMessage(MessagesConfig.MODERATOR_MOD_ON.parse());
				} else {
					player.sendMessage(MessagesConfig.MODERATOR_MOD_OFF.parse());
				}
				
				if(PlayerManager.switchPlayerData(this.player, !state)) {
					player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(" "));
					return true;
				} return false;
			} else {
				player.sendMessage(MessagesConfig.NO_PERMISSION.parse());
			}
		} else {
			player.sendMessage(MessagesConfig.OFFLINE_PLAYER.parse());
		}
		return false;	
	}
	
	public boolean on() {
		if(player != null) {
			if(player.hasPermission("moderation.toggle")) {				
				player.sendMessage(MessagesConfig.MODERATOR_MOD_ON.parse());
				
				if(PlayerManager.switchPlayerData(this.player, true)) {
					return true;
				} return false;
			} else {
				player.sendMessage(MessagesConfig.NO_PERMISSION.parse());
			}
		} else {
			player.sendMessage(MessagesConfig.OFFLINE_PLAYER.parse());
		}
		return false;	
	}
	
	public boolean forceOn() {
		if(player != null) {
			if(player.hasPermission("moderation.toggle")) {
				player.sendMessage(MessagesConfig.AUTOMATICLY_SWITCHED.parse());
				
				if(PlayerManager.switchPlayerData(this.player, true)) {
					return true;
				} return false;
			} else {
				player.sendMessage(MessagesConfig.NO_PERMISSION.parse());
			}
		} else {
			player.sendMessage(MessagesConfig.OFFLINE_PLAYER.parse());
		}
		return false;	
	}
	
	public boolean off() {
		if(player != null) {
			if(player.hasPermission("moderation.toggle")) {		
				player.sendMessage(MessagesConfig.MODERATOR_MOD_OFF.parse());
				
				if(PlayerManager.switchPlayerData(this.player, false)) {
					player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));
					return true;
				} return false;
			} else {
				player.sendMessage(MessagesConfig.NO_PERMISSION.parse());
			}
		} else {
			player.sendMessage(MessagesConfig.OFFLINE_PLAYER.parse());
		}
		return false;	
	}

}
