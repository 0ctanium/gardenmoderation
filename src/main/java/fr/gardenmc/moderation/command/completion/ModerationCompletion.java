package fr.gardenmc.moderation.command.completion;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public class ModerationCompletion implements TabCompleter {

	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> completion = new ArrayList<String>();
		if(sender.hasPermission("moderation.reload")) completion.add("reload");
		if(sender.hasPermission("moderation.toggle")) {
			completion.add("toggle");
			completion.add("on");
			completion.add("off");
		}
		return null;
	}

}
