package fr.gardenmc.moderation.manager;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import fr.gardenmc.moderation.Main;
import fr.gardenmc.moderation.config.ModerationConfig;
import fr.gardenmc.moderation.util.player.InventoryUtil;
import fr.gardenmc.moderation.util.player.PlayerData;
import fr.gardenmc.moderation.util.player.PlayerUtil;
import fr.gardenmc.moderation.util.player.PotionUtil;

public class PlayerManager {

	private static Map<UUID, PlayerData> players = new HashMap<UUID, PlayerData>();
	private static Map<UUID, CommandWrapper> refusedCommands = new HashMap<UUID, CommandWrapper>();
	
	public static void load() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			PlayerData playerData = new PlayerData(player);
			if(playerData.getData() != null && playerData.getFile() != null) {
				players.put(player.getUniqueId(), playerData);
			}
		});
	}
	
	public static void addPlayerData(UUID uuid, PlayerData data) {
		players.put(uuid, data);
	}
	
	/**
	 * 
	 * @param uuid
	 * @param force if file doesn't exists, create it
	 * @return PlayerData
	 */
	public static PlayerData getPlayerData(UUID uuid, boolean force) {
		PlayerData playerData = players.get(uuid);
		
		if(playerData == null || playerData.getFile() == null || !playerData.getFile().exists()) {
			if(!force) return null;
			try {
				new File(Main.getInstance().getDataFolder(), "data/" + uuid + ".yml").createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			File file = new File(Main.getInstance().getDataFolder(), "data/" + uuid + ".yml");
			YamlConfiguration data = YamlConfiguration.loadConfiguration(file);
			
			Map<String, Object> defaultValues = new HashMap<String, Object>();
			Map<String, Object> savedLocation =  new HashMap<String, Object>();
			defaultValues.put("uuid", uuid.toString());
			defaultValues.put("state", false);
				savedLocation.put("x", 0);
				savedLocation.put("y", 0);
				savedLocation.put("z", 0);
				savedLocation.put("pitch", 0);
				savedLocation.put("yaw", 0);
				savedLocation.put("w", 0);
			defaultValues.put("location", savedLocation);
			defaultValues.put("gamemode", ModerationConfig.defaultGamemode().toString());
			defaultValues.put("xp", 0);
			defaultValues.put("food", 20);
			defaultValues.put("saturation", 10.0);
			defaultValues.put("health", 20.0);
			defaultValues.put("inv", "");
			defaultValues.put("ender", "");
			defaultValues.put("potions", "");
			
			data.createSection("data", defaultValues);
			try {
				data.save(file);
			} catch (IOException e) {
				e.printStackTrace();
				
			}
		}
		
		return playerData;
	}
	
	public static boolean switchPlayerData(Player player, boolean state) {
		PlayerData playerData = PlayerManager.getPlayerData(player.getUniqueId(), true);
		FileConfiguration data = playerData.getData();
		
		Location loc = player.getLocation();		
		Map<String, Object> newData =  new HashMap<String, Object>();
		Map<String, Object> savedLocation =  new HashMap<String, Object>();
		newData.put("uuid", player.getUniqueId().toString());
		newData.put("state", state);
			savedLocation.put("x", loc.getX());
			savedLocation.put("y", loc.getY());
			savedLocation.put("z", loc.getZ());
			savedLocation.put("pitch", loc.getPitch());
			savedLocation.put("yaw", loc.getYaw());
			savedLocation.put("w", loc.getWorld().getName());
		newData.put("location", savedLocation);
		newData.put("xp", PlayerUtil.getTotalExperience(player));
		newData.put("food", player.getFoodLevel());
		newData.put("saturation", player.getSaturation());
		newData.put("health", player.getHealth());
		newData.put("gamemode", player.getGameMode().toString());
		newData.put("inv", InventoryUtil.toBase64(player.getInventory()));
		newData.put("ender", InventoryUtil.toBase64(player.getEnderChest()));
		newData.put("potions", PotionUtil.toBase64(player.getActivePotionEffects()));
		
		player.getInventory().setContents(InventoryUtil.fromBase64(data.getString("data.inv")));
		player.getEnderChest().setContents(InventoryUtil.fromBase64(data.getString("data.ender")));
	    for(PotionEffect effect : player.getActivePotionEffects()){
	        player.removePotionEffect(effect.getType());
	    }
		player.addPotionEffects(PotionUtil.fromBase64(data.getString("data.potions")));
		player.setHealth(data.getDouble("data.health"));
		player.setFoodLevel(data.getInt("data.food"));
		player.setSaturation((float) data.getDouble("data.saturation"));
		player.setGameMode(GameMode.valueOf(data.getString("data.gamemode")));
		PlayerUtil.setTotalExperience(player, (float) data.getDouble("data.xp"));
		
		data.createSection("data", newData);		
		try {
			playerData.saveDataSync();
		} catch (Exception e) {
	        e.printStackTrace();
			return false;
		}
		
		players.replace(player.getUniqueId(), players.get(player.getUniqueId()), new PlayerData(player.getUniqueId()));
		return true;
	}
	
	public static CommandWrapper getRefusedCommand(UUID uuid) {
		return refusedCommands.get(uuid);
	}
	
	public static void setLastRefusedCommand(UUID uuid, String command, long timestamp) {
		refusedCommands.put(uuid, new CommandWrapper(command, timestamp));
	}

	public static void setLastRefusedCommand(UUID uuid, CommandWrapper wrapper) {
		refusedCommands.put(uuid, wrapper);
	}
	
	public static Map<UUID, PlayerData> getPlayers() {
		return players;
	}
	
	public static class CommandWrapper {
		
		private String command = null;
		private long timestamp = 0;
		
		public CommandWrapper(String command, long timestamp) {
			this.command = command;
			this.timestamp = timestamp;
		}
		
		public String getCommand() {
			return this.command;
		}
		
		public long getTimestamp() {
			return this.timestamp;
		}
	}
}
