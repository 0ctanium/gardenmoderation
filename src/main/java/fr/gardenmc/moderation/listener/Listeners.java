package fr.gardenmc.moderation.listener;

import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import fr.gardenmc.moderation.command.moderation.Toggle;
import fr.gardenmc.moderation.config.MessagesConfig;
import fr.gardenmc.moderation.config.ModerationConfig;
import fr.gardenmc.moderation.manager.PlayerManager;
import fr.gardenmc.moderation.manager.PlayerManager.CommandWrapper;
import fr.gardenmc.moderation.util.player.PlayerData;

public class Listeners implements Listener {

	@EventHandler
	public void onConnect(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if(PlayerManager.getPlayerData(player.getUniqueId(), false) != null) {
			PlayerData playerData = new PlayerData(player);
			if(playerData.getData() != null && playerData.getFile() != null) {
				PlayerManager.addPlayerData(player.getUniqueId(), playerData);
			}
		}
	}
	

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCommandPreProcess(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		if(event.getPlayer().hasPermission("moderation.moderation")) {
			PlayerData data = PlayerManager.getPlayerData(player.getUniqueId(), false);
			if(data == null) return;
			if(!data.getData().getBoolean("data.state")) {

				String command = event.getMessage().toLowerCase().split(" ")[0];
				
				final String regex = "^\\/(.*\\:)?([\\w]+)";
				final Pattern pattern = Pattern.compile(regex);
				final Matcher matcher = pattern.matcher(command);
				
				while (matcher.find()) {
					if(matcher.group(2) != null) {
						if(ModerationConfig.blacklistedCommands().contains(matcher.group(2)) || ModerationConfig.blacklistedCommands().contains(command.substring(1))) {
							CommandWrapper refused = PlayerManager.getRefusedCommand(player.getUniqueId());
							if(refused != null && ModerationConfig.forceCommandSwitch()) {
								if(refused.getCommand().equalsIgnoreCase(command.substring(1)) && refused.getTimestamp() >= getTimestamp() - ModerationConfig.forceCommandSwitchDelay() * 1000) {
									if(player.hasPermission("moderation.toggle")) {
										new Toggle(event.getPlayer()).forceOn();
									} else {
										player.sendMessage(MessagesConfig.NO_PERMISSION.parse());
									}
									return;
								}
							}
							event.setCancelled(true);
							PlayerManager.setLastRefusedCommand(player.getUniqueId(), command.substring(1), getTimestamp());
							event.getPlayer().sendMessage(MessagesConfig.CANNOT_COMMAND_PLAYMOD.parse());
						}
					}
				}
				

			};
		}
	}
	
	private long getTimestamp() {
		return new Timestamp(System.currentTimeMillis()).getTime();
	}
	
}
